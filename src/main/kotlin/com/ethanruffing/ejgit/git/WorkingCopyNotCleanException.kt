package com.ethanruffing.ejgit.git


import java.lang.Exception

/**
 * An exception that is thrown when an attempt is made to merge, branch, or
 * checkout when the working copy contains uncommitted changes.
 *
 * @author Ethan Ruffing
 * @since 2016-02-12
 */
class WorkingCopyNotCleanException : Exception {
    constructor() : super() {}
    constructor(message: String?) : super(message) {}
    constructor(message: String?, cause: Throwable?) : super(message, cause) {}
    constructor(cause: Throwable?) : super(cause) {}
}