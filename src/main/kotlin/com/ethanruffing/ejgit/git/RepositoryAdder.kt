package com.ethanruffing.ejgit.git

import javafx.beans.property.*
import java.io.IOException
import org.eclipse.jgit.api.errors.GitAPIException
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import java.io.File
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider
import org.eclipse.jgit.lib.RepositoryCache
import org.eclipse.jgit.util.FS
import tornadofx.*

/**
 * A class to use for constructing and adding repositories to the system.
 *
 * @author Ethan Ruffing
 * @since 2016-02-03
 */
class RepositoryAdder(val manager: RepositoryManager) {

    val SourceUrlProperty = SimpleStringProperty()
    val ValidWorkingCopyTextProperty = SimpleStringProperty()
    val ValidNewLocationTextProperty = SimpleStringProperty()
    val ActionValidProperty = SimpleBooleanProperty()

    val usernameProperty = object : SimpleStringProperty() {
        override fun set(value: String) {
            super.set(value)
            manager.prefs!!.put("cloneUsername", value)
        }
    }
    val passwordProperty = SimpleStringProperty()
    val loginRequiredProperty = object : SimpleBooleanProperty() {
        override fun set(value: Boolean) {
            super.set(value)
            manager.prefs!!.put("cloneLoginRequired", value)
        }
    }

    var SourceUrl by SourceUrlProperty
    var ValidWorkingCopyText by ValidWorkingCopyTextProperty
    var ValidNewLocationText by ValidNewLocationTextProperty
    var ActionValid by ActionValidProperty
    var username: String by usernameProperty
    var password: String by passwordProperty
    var loginRequired: Boolean by loginRequiredProperty

    val DestinationProperty = object : SimpleStringProperty() {
        override fun set(value: String) {
            super.set(value)
            val f = File(value)
            if (RepositoryCache.FileKey.isGitRepository(
                    File(
                        value,
                        ".git"
                    ), FS.DETECTED
                )
            ) {
                ValidWorkingCopyText = "This is a valid git repository."
                if (repoAddType === RepoAddType.ADD) {
                    ActionValid = (true)
                }
            } else {
                ValidWorkingCopyText = "This is NOT a valid git repository."
                if (repoAddType === RepoAddType.ADD) {
                    ActionValid = (false)
                }
            }
            if (f.exists() && f.isDirectory && f.list().isEmpty()) {
                ValidNewLocationText = "This is a valid empty directory."
                if (repoAddType === RepoAddType.CLONE
                    || repoAddType === RepoAddType.NEW
                ) {
                    ActionValid = (true)
                }
            } else {
                ValidNewLocationText = "This is NOT a valid empty directory."
                if (repoAddType === RepoAddType.CLONE
                    || repoAddType === RepoAddType.NEW
                ) {
                    ActionValid = (false)
                }
            }
        }
    }
    var Destination: String by DestinationProperty


    private var _RepoAddType = RepoAddType.CLONE

    var repoAddType: RepoAddType
        get() = _RepoAddType
        set(value) {
            this._RepoAddType = value
            val f = File(Destination)
            if (RepositoryCache.FileKey.isGitRepository(
                    File(
                        Destination,
                        ".git"
                    ), FS.DETECTED
                )
            ) {
                ValidWorkingCopyText = "This is a valid git repository."
                if (repoAddType === RepoAddType.ADD) {
                    ActionValid = (true)
                }
            } else {
                ValidWorkingCopyText = ("This is NOT a valid git repository.")
                if (repoAddType === RepoAddType.ADD) {
                    ActionValid = (false)
                }
            }
            if (f.exists() && f.isDirectory && f.list().size <= 0) {
                ValidNewLocationText = ("This is a valid empty directory.")
                if (repoAddType === RepoAddType.CLONE
                    || repoAddType === RepoAddType.NEW
                ) {
                    ActionValid = (true)
                }
            } else {
                ValidNewLocationText = ("This is NOT a valid empty directory.")
                if (repoAddType === RepoAddType.CLONE
                    || repoAddType === RepoAddType.NEW
                ) {
                    ActionValid = false
                }
            }
        }

    @Throws(IOException::class, GitAPIException::class)
    fun add() {
        // TODO: Add some dialog that shows that the system is working
        if (repoAddType === RepoAddType.ADD) {
            val builder = FileRepositoryBuilder()
            val repository = builder.setGitDir(
                File(
                    Destination,
                    ".git"
                )
            )
                .readEnvironment()
                .findGitDir()
                .build()
            manager.Repositories.add(repository)
        } else if (repoAddType === RepoAddType.CLONE) {
            // TODO: Implement clone methods for ssh
            val result: Git
            result = if (!loginRequired) {
                Git.cloneRepository()
                    .setURI(SourceUrl)
                    .setDirectory(File(Destination))
                    .call()
            } else {
                Git.cloneRepository()
                    .setURI(SourceUrl)
                    .setDirectory(File(Destination))
                    .setCredentialsProvider(
                        UsernamePasswordCredentialsProvider(
                            username,
                            password
                        )
                    )
                    .call()
            }
            manager.Repositories.add(result.repository)
        } else if (repoAddType === RepoAddType.NEW) {
            val git = Git.init()
                .setDirectory(File(Destination))
                .call()
            manager.Repositories.add(git.repository)
            // TODO: Fix display of repo contents after initializing empty repo
        }
        manager.storeRepoList()
    }

    init {
        Destination = ""
        repoAddType = RepoAddType.CLONE
        username = manager.prefs?.getString("cloneUsername", "") ?: ""
        loginRequired = manager.prefs?.getBoolean("cloneLoginRequired", false) ?: false
    }
}