package com.ethanruffing.ejgit.git

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import java.util.prefs.Preferences
import tornadofx.*

/**
 * A class to use for building a commit.
 *
 * @author Ethan Ruffing
 * @since 2016-02-12
 */
class CommitBuilder(val manager: RepositoryManager) {
    private val prefs = Preferences.userNodeForPackage(javaClass)

    /**
     * The commit message for to use for the commit.
     */
    val commitMessageProperty = object : SimpleStringProperty() {
        override fun set(value: String) {
            super.set(value);
            prefs.put("authorName", value);
        }
    }
    var commitMessage: String by commitMessageProperty

    /**
     * The name of the commit's author.
     */
    val authorNameProperty = object : SimpleStringProperty() {
        override fun set(value: String) {
            super.set(value)
            prefs.put("authorName", value)
        }
    }
    var authorName: String by authorNameProperty

    /**
     * The commit author's email address.
     */
    val authorEmailProperty = object : SimpleStringProperty() {
        override fun set(value: String) {
            super.set(value)
            prefs.put("authorEmail", value)
        }
    }
    var authorEmail: String by authorEmailProperty

    /**
     * The committer's name.
     */
    val committerNameProperty = object : SimpleStringProperty() {
        override fun set(value: String) {
            super.set(value)
            prefs.put("committerName", value)
        }
    }
    var committerName: String by committerNameProperty

    /**
     * The committer's email address.
     */
    val committerEmailProperty = object : SimpleStringProperty() {
        override fun set(value: String) {
            super.set(value)
            prefs.put("committerEmail", value)
        }
    }
    var committerEmail: String by committerEmailProperty

    /**
     * Whether the commit should be an amendment of the previous commit.
     */
    val amendProperty = object : SimpleBooleanProperty() {
        override fun set(value: Boolean) {
            super.set(value)
            prefs.putBoolean("commitAmend", value)
        }
    }
    var amend: Boolean by amendProperty

    init {
        manager.SelectedRepositoryProperty.addListener { o, old, new ->
            updateFromSettings()
        }

        updateFromSettings()
    }

    private fun updateFromSettings() {
        //TODO: Change to set and retrieve author properties in git settings
        authorName = prefs["authorName", ""]
        authorEmail = prefs["authorEmail", ""]
        committerName = prefs["committerName", ""]
        committerEmail = prefs["committerEmail", ""]
        amend = prefs.getBoolean("commitAmend", false)
    }
}
