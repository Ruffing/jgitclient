package com.ethanruffing.ejgit.gui

import tornadofx.launch

fun main(args: Array<String>) {
    launch<EJGitApp>(args)
}
