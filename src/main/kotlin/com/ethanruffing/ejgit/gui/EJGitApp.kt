package com.ethanruffing.ejgit.gui

import com.ethanruffing.ejgit.gui.view.MainView
import javafx.scene.Scene
import tornadofx.App
import tornadofx.UIComponent

class EJGitApp : App(MainView::class, Styles::class) {
    lateinit var primaryScene: Scene
    override fun createPrimaryScene(view: UIComponent): Scene {
        primaryScene = Scene(view.root)

//        var jMetro = JMetro(primaryScene, Style.LIGHT)


        return primaryScene
    }
}