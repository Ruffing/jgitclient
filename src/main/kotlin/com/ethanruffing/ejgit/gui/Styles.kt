package com.ethanruffing.ejgit.gui

import javafx.scene.text.FontWeight
import org.kordamp.bootstrapfx.BootstrapFX
import tornadofx.*

class Styles : Stylesheet() {
    companion object {
        val heading by cssclass()
        val btn by cssclass()

        init {
            importStylesheet(BootstrapFX.bootstrapFXStylesheet())
        }

    }

    init {
        label and heading {
            padding = box(10.px)
            fontSize = 20.px
            fontWeight = FontWeight.BOLD
        }

    }
}