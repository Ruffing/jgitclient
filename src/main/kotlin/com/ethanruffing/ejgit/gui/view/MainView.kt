package com.ethanruffing.ejgit.gui.view

import com.ethanruffing.ejgit.git.RepositoryManager
import com.ethanruffing.ejgit.git.WorkingCopyNotCleanException
import com.ethanruffing.ejgit.gui.applyClassRecursively
import javafx.geometry.Orientation
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.Priority
import javafx.stage.Stage
import org.apache.commons.lang.exception.ExceptionUtils
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.errors.GitAPIException
import org.slf4j.LoggerFactory
import tornadofx.*
import java.io.IOException


class MainView : View("EJGit") {
    private val logger = LoggerFactory.getLogger(javaClass)
    private var manager = RepositoryManager();
    private val stage: Stage
        get() = root.scene.window as Stage
    override val root = vbox {
        menubar {
            vboxConstraints {
                vGrow = Priority.NEVER
            }
            menu("_File") {
                item("E_xit").action { mvClose() }
            }
            menu("_Settings") {
                item("_System").action { systemSettings() }
            }

        }
        toolbar {
            vboxConstraints {
                vGrow = Priority.NEVER
            }
            button("Add Repository") {
                addClass("btn")
            }.action { addRepository() }
            separator(Orientation.VERTICAL)
            button("Fetch") { enableWhen(manager.SelectedRepositoryProperty.isNotNull) }.action { fetch() }
            button("Pull") { enableWhen(manager.SelectedRepositoryProperty.isNotNull) }.action { pull() }
            button("Push") { enableWhen(manager.SelectedRepositoryProperty.isNotNull) }.action { push() }
            separator(Orientation.VERTICAL)
            button("Branch") { enableWhen(manager.SelectedRepositoryProperty.isNotNull) }.action { showBranchDlg() }
            button("Tag") { enableWhen(manager.SelectedRepositoryProperty.isNotNull) }.action { showTagDlg() }
            button("Terminal") {
                enableWhen(manager.SelectedRepositoryProperty.isNotNull)
            }.action { showTerminal() }
        }
        anchorpane {
            vboxConstraints {
                vGrow = Priority.ALWAYS
            }
            splitpane {
                anchorpaneConstraints {
                    bottomAnchor = 0.0
                    leftAnchor = 0.0
                    rightAnchor = 0.0
                    topAnchor = 0.0
                }
                anchorpane {
                    listview(manager.RepositoriesProperty) {
                        id = "repositoryListView"
                        anchorpaneConstraints {
                            bottomAnchor = 0.0
                            leftAnchor = 0.0
                            rightAnchor = 0.0
                            topAnchor = 0.0
                        }
                        bindSelected(manager.SelectedRepositoryProperty);
                        contextmenu {
                            item("Remove Repository").action { removeSelectedRepo(); }
                            item("Delete Repository").action { deleteSelectedRepo(); }
                        }

                        cellFormat {
                            text = if (it == null)
                                ""
                            else
                                try {
                                    val display = (it.workTree.name + " ["
                                            + it.branch + "]")
                                    display
                                } catch (e: IOException) {
                                    it.workTree.name
                                }

                            style = if (it == manager.SelectedRepository)
                                style + "$style-fx-font-weight: bold;"
                            else
                                "$style-fx-font-weight: normal;"
                        }
                    }
                }
                anchorpane {
                    listview(manager.BranchesProperty) {
                        id = "branchesListView"
                        anchorpaneConstraints {
                            bottomAnchor = 0.0
                            leftAnchor = 0.0
                            rightAnchor = 0.0
                            topAnchor = 0.0
                        }
                        contextmenu {
                            item("Merge into current branch").action { mergeBranch(); }
                            item("Rename branch").action { renameBranch(); }
                            item("Delete branch").action { deleteBranch(); }
                        }
                        bindSelected(manager.SelectedBranchProperty)

                        cellFormat {
                            text = if (it == null)
                                ""
                            else
                                it.name
                                    .replaceFirst("refs/heads/".toRegex(), "")
                                    .replaceFirst("refs/remotes/".toRegex(), "")

                            try {
                                style =
                                    if (text == manager.SelectedRepository.branch)
                                        "$style-fx-font-weight: bold;"
                                    else
                                        "$style-fx-font-weight: normal;"
                            } catch (e: IOException) {
                                logger.error("Error setting branch style.", e)
                            }
                        }
                        onDoubleClick { checkoutSelectedBranch() }
                    }
                }
                anchorpane {
                    var repView = RepositoryView(manager)

                    AnchorPane.setTopAnchor(repView.root, 0.0);
                    AnchorPane.setBottomAnchor(repView.root, 0.0);
                    AnchorPane.setLeftAnchor(repView.root, 0.0);
                    AnchorPane.setRightAnchor(repView.root, 0.0)

                    add(repView)
                }
                setDividerPositions(0.125, 0.25)
            }
        }

        applyClassRecursively(Button::class, "btn")
        applyClassRecursively(Label::class, "lbl")
//        applyClassRecursively(Pane::class, "panel")
    }

    private fun checkoutSelectedBranch() {
        try {
            manager.checkoutSelectedBranch()
        } catch (e: WorkingCopyNotCleanException) {
            val alert = Alert(Alert.AlertType.WARNING)
            alert.title = "Uncommitted Changes"
            alert.headerText = (
                    "The working copy contains changes that have not been "
                            + "committed.")
            alert.contentText = (
                    "Please commit or discard all changes before checking out "
                            + "another branch.")
            alert.showAndWait()
        } catch (e: GitAPIException) {
            e.printStackTrace()
            val alert = Alert(Alert.AlertType.ERROR)
            alert.title = "Error Checking Out"
            alert.headerText = (
                    "An error occurred while attempting to check out the "
                            + "selected branch.")
            alert.contentText = """
                Stack Trace:
                ${ExceptionUtils.getStackTrace(e)}
                """.trimIndent()
        }
    }

    private fun deleteBranch() {
        var currentBranch = ""
        try {
            currentBranch = manager.CurrentBranchName
        } catch (e: IOException) {
            // An IOException is ok here.
        }

        if (manager.SelectedBranchName == currentBranch) {
            val alert = Alert(Alert.AlertType.ERROR)
            alert.title = "Delete Branch"
            alert.headerText = "Can not delete current branch."
            alert.contentText = "You must first checkout another branch."
            alert.showAndWait()
            return
        } else {
            val alert = Alert(Alert.AlertType.CONFIRMATION)
            alert.title = "Delete Branch"
            alert.headerText = "Are you sure?"
            alert.contentText = ("Are you sure you want to delete the branch '"
                    + manager.SelectedBranchName + "'?")
            val delForceBt = ButtonType("Delete (Force)")
            val delBt = ButtonType("Delete")
            alert.buttonTypes.setAll(delForceBt, delBt, ButtonType.CANCEL)
            val result = alert.showAndWait()
            if (result.get() == delBt || result.get() == delForceBt) {
                val git = Git(manager.SelectedRepository)
                try {
                    git.branchDelete()
                        .setBranchNames(manager.SelectedBranchName)
                        .setForce(result.get() == delForceBt)
                        .call()
                } catch (e: GitAPIException) {
                    val alert1 = Alert(Alert.AlertType.ERROR)
                    alert1.title = "Delete Branch"
                    alert1.headerText = "Error when deleting branch."
                    alert1.contentText = ExceptionUtils.getStackTrace(e)
                    alert1.showAndWait()
                }
            }
        }

        manager.refreshLists()
    }

    private fun renameBranch() {
        val dialog = TextInputDialog(manager.SelectedBranchName)

        dialog.title = "Rename Branch"
        dialog.headerText = "New branch name?"
        dialog.contentText = "Enter the new name for the branch:"

        val result = dialog.showAndWait()
        if (result.isPresent) {
            val git = Git(manager.SelectedRepository)
            try {
                git.branchRename()
                    .setOldName(manager.SelectedBranchName)
                    .setNewName(result.get())
                    .call()
            } catch (e: GitAPIException) {
                val alert = Alert(Alert.AlertType.ERROR)
                alert.title = "Branch Rename Failed"
                alert.headerText = "Failed to rename the selected branch."
                alert.contentText = """
            Stack Trace:
            ${ExceptionUtils.getStackTrace(e)}
            """.trimIndent()
                alert.showAndWait()
            }
        }
        manager.refreshLists()
    }

    private fun mergeBranch() {
        TODO("Not yet implemented")
    }


    private fun deleteSelectedRepo() {
        var alert = Alert(Alert.AlertType.CONFIRMATION)
        alert.title = "Delete Repository";
        alert.headerText = "Are you sure?";
        alert.contentText =
            "You are about to delete this repository from the disk. This action cannot be undone. Are you sure you want to do this?";
        var result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            try {
                manager.deleteRepository(manager.SelectedBranchIndex);
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun removeSelectedRepo() {
        manager.Repositories.remove(manager.SelectedRepository)
        manager.storeRepoList();
    }

    private fun showBranchDlg() {
        dialog("New Branch") {
            stage.scene = Scene(BranchDlg(manager).root)
        }
    }

    private fun showTagDlg() {
        TODO("Not yet implemented")
    }

    private fun showTerminal() {
        TerminalWin(manager).openWindow()
    }

    private fun fetch() {
        try {
            manager.fetch()
        } catch (e: GitAPIException) {
            logger.error("Error pushing", e)
        } catch (e: IOException) {
            logger.error("Error pushing", e)
        }
    }

    private fun pull() {
        try {
            manager.pull()
        } catch (e: GitAPIException) {
            logger.error("Error pushing", e)
        } catch (e: IOException) {
            logger.error("Error pushing", e)
        }
    }

    private fun push() {
        try {
            manager.push()
        } catch (e: GitAPIException) {
            logger.error("Error pushing", e)
        } catch (e: IOException) {
            logger.error("Error pushing", e)
        }
    }

    private fun addRepository() {
        dialog("Add Repository") {
            stage.scene = Scene(AddRepositoryDlg(manager).root)
        }
    }

    private fun systemSettings() {
        TODO("Not yet implemented")
    }

    private val repositoryListView: ListView<*>
        get() = root.scene.lookup("#repositoryListView") as ListView<*>
    private val branchesListView: ListView<*>
        get() = root.scene.lookup("#repositoryListView") as ListView<*>

    init {
    }

    override fun onDock() {
        super.onDock()
        loadPreviousView()
    }

    override fun onBeforeShow() {
        super.onBeforeShow()
        stage.setOnCloseRequest { mvClose() }

    }

    fun mvClose() {
        storeView()
        manager.close()
        close()
    }

    /**
     * Stores all component sizes and related data to preferences in order to
     * remember them for a future session.
     */
    private fun storeView() {
        manager.prefs?.put("stageWidth", stage.width)
        manager.prefs?.put("stageHeight", stage.height)
        manager.prefs?.put("stageX", stage.x)
        manager.prefs?.put("stageY", stage.y)
        manager.prefs?.put("maximized", stage.isMaximized)
        manager.prefs?.put("repoListViewWidth", repositoryListView.width)
        manager.prefs?.put("branchListViewWidth", branchesListView.width)
    }

    /**
     * Loads the component sizes and related data from preferences and applies
     * them to the current session.
     */
    private fun loadPreviousView() {
        if (manager.prefs == null) return;
        stage.width = manager.prefs!!.getDouble("stageWidth", stage.width)
        stage.height = manager.prefs!!.getDouble("stageHeight", stage.height)
        stage.x = manager.prefs!!.getDouble("stageX", stage.x)
        stage.y = manager.prefs!!.getDouble("stageY", stage.y)
        stage.isMaximized = manager.prefs!!.getBoolean("maximized", stage.isMaximized)
        repositoryListView.prefWidth = manager.prefs!!.getDouble("repoListViewWidth", repositoryListView.prefWidth)
        branchesListView.prefWidth = manager.prefs!!.getDouble("branchListViewWidth", branchesListView.prefWidth)
    }
}
