package com.ethanruffing.ejgit.gui.view

import com.ethanruffing.ejgit.git.RepositoryManager
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.HPos
import javafx.scene.control.Alert
import javafx.scene.layout.Priority
import org.apache.commons.lang.exception.ExceptionUtils
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.errors.GitAPIException
import tornadofx.*

class BranchDlg(val manager: RepositoryManager) : View("My View") {
    private val newBranchNameProperty = SimpleStringProperty()
    private var newBranchName by newBranchNameProperty
    override val root = vbox {
        gridpane {
            label("New branch name:") {
                gridpaneConstraints {
                    marginBottom = 5.0
                    marginLeft = 5.0
                    marginRight = 5.0
                    marginTop = 5.0
                }
            }
            textfield {
                gridpaneConstraints {
                    columnIndex = 1
                    marginBottom = 5.0
                    marginLeft = 5.0
                    marginRight = 5.0
                    marginTop = 5.0
                }
                textProperty().bindBidirectional(newBranchNameProperty)
            }.action { ok() }

            constraintsForColumn(0).halignment = HPos.RIGHT
            constraintsForColumn(0).hgrow = Priority.SOMETIMES
            constraintsForColumn(0).minWidth = 10.0
            constraintsForColumn(1).hgrow = Priority.ALWAYS
            constraintsForColumn(1).minWidth = 10.0
            constraintsForColumn(1).prefWidth = 100.0
            constraintsForRow(0).minHeight = 10.0
            constraintsForRow(0).vgrow = Priority.SOMETIMES
            constraintsForRow(1).minHeight = 10.0
            constraintsForRow(1).prefHeight = 30.0
            constraintsForRow(1).vgrow = Priority.ALWAYS

            vboxConstraints {
                paddingBottom = 5.0
                paddingLeft = 5.0
                paddingRight = 5.0
                paddingTop = 10.0
            }
        }
        gridpane {
            button("OK") {
                isDefaultButton = true
                gridpaneConstraints {
                    columnIndex = 1
                    marginBottom = 5.0
                    marginLeft = 5.0
                    marginRight = 5.0
                    marginTop = 5.0
                }
            }.action { ok() }
            button("Cancel") {
                isCancelButton = true
                gridpaneConstraints {
                    columnIndex = 2
                    marginBottom = 5.0
                    marginLeft = 5.0
                    marginRight = 5.0
                    marginTop = 5.0
                }
            }.action { close() }

            constraintsForColumn(0).hgrow = Priority.ALWAYS
            constraintsForColumn(0).minWidth = 10.0
            constraintsForColumn(0).prefWidth = 100.0
            constraintsForColumn(1).hgrow = Priority.SOMETIMES
            constraintsForColumn(1).minWidth = 10.0
            constraintsForColumn(2).hgrow = Priority.SOMETIMES
            constraintsForColumn(2).minWidth = 10.0
            constraintsForRow(0).minHeight = 10.0
            constraintsForRow(0).vgrow = Priority.SOMETIMES
        }
    }

    private fun ok() {
        val git = Git(manager.SelectedRepository)
        try {
            git.checkout()
                .setName(newBranchName)
                .setCreateBranch(true)
                .call()
            manager.refreshLists()
            close()
        } catch (e: GitAPIException) {
            e.printStackTrace()
            val alert = Alert(Alert.AlertType.ERROR)
            alert.title = "Error Creating Branch"
            alert.headerText = "An error occurred while creating the new branch."
            alert.contentText = """
                Stack Trace:
                ${ExceptionUtils.getStackTrace(e)}
                """.trimIndent()
            alert.showAndWait()
        }
    }
}
