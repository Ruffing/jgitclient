package com.ethanruffing.ejgit.gui.view

import com.ethanruffing.ejgit.git.CommitBuilder
import com.ethanruffing.ejgit.git.RepositoryManager
import com.ethanruffing.ejgit.gui.applyClassRecursively
import javafx.beans.property.BooleanProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.value.ObservableValue
import javafx.geometry.HPos
import javafx.geometry.Orientation
import javafx.geometry.VPos
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.control.cell.CheckBoxListCell
import javafx.scene.layout.Priority
import javafx.scene.text.TextAlignment
import javafx.util.Callback
import org.eclipse.jgit.api.errors.GitAPIException
import tornadofx.*
import java.util.logging.Level
import java.util.logging.Level.SEVERE

class StagingView(val manager: RepositoryManager) : View("Staging") {
    private var commitBuilder = CommitBuilder(manager)

    override val root = anchorpane {
        splitpane(Orientation.VERTICAL) {
            anchorpaneConstraints {
                leftAnchor = 0.0
                topAnchor = 0.0
                rightAnchor = 0.0
                bottomAnchor = 0.0
            }
            anchorpane {
                splitpane() {
                    prefHeight = 115.0
                    anchorpaneConstraints {
                        leftAnchor = 0.0
                        topAnchor = 0.0
                        rightAnchor = 0.0
                        bottomAnchor = 0.0
                    }
                    anchorpane {
                        vbox {
                            anchorpaneConstraints {
                                leftAnchor = 0.0
                                topAnchor = 0.0
                                rightAnchor = 0.0
                                bottomAnchor = 0.0
                            }
                            toolbar {
                                button("Stage All").action { stageAll(); }
                                button("Unstage All").action { unstageAll(); }
                            }
                            splitpane(Orientation.VERTICAL) {
                                anchorpane {
                                    listview(manager.StagedFilesProperty) {
                                        anchorpaneConstraints {
                                            leftAnchor = 0.0
                                            topAnchor = 0.0
                                            rightAnchor = 0.0
                                            bottomAnchor = 0.0
                                        }
                                        id = "stagedFilesView"
                                        selectionModel.selectedIndexProperty().addListener { _, _, new ->
                                            manager.SelectedStagedFileIndex = new.toInt()
                                        }

                                        cellFactory = CheckBoxListCell.forListView { item ->
                                            val observable: BooleanProperty = SimpleBooleanProperty(true)
                                            observable.addListener { _: ObservableValue<out Boolean?>?, _: Boolean?, isNowSelected: Boolean? ->
                                                if (!isNowSelected!!) try {
                                                    manager.unstage(item)
                                                } catch (e: GitAPIException) {
                                                    log.log(SEVERE, "Error unstaging file.", e)
                                                }
                                            }
                                            observable
                                        }
                                    }
                                }
                                anchorpane {
                                    listview(manager.UnstagedFilesProperty)
                                    {
                                        anchorpaneConstraints {
                                            leftAnchor = 0.0
                                            topAnchor = 0.0
                                            rightAnchor = 0.0
                                            bottomAnchor = 0.0
                                        }
                                        id = "unstagedFilesView"
                                        selectionModel.selectedIndexProperty().addListener { _, _, new ->
                                            manager.SelectedUnstagedFileIndex = new.toInt()
                                        }
                                        cellFactory = CheckBoxListCell.forListView { item ->
                                            val observable: BooleanProperty = SimpleBooleanProperty(false)
                                            observable.addListener { _: ObservableValue<out Boolean>?, _: Boolean?, isNowSelected: Boolean ->
                                                if (isNowSelected) try {
                                                    manager.stage(item)
                                                } catch (e: GitAPIException) {
                                                    log.log(SEVERE, "Error staging file.", e)
                                                }
                                            }
                                            observable
                                        }
                                    }
                                }
                                setDividerPositions(0.5)
                            }
                        }
                    }
                    anchorpane {
                        listview(manager.DiffTextListProperty) {
                            anchorpaneConstraints {
                                leftAnchor = 0.0
                                topAnchor = 0.0
                                rightAnchor = 0.0
                                bottomAnchor = 0.0
                            }
                            id = "diffTextListView"
                            cellFactory = Callback {
                                object : ListCell<String?>() {
                                    protected fun updateItem(item: String, empty: Boolean) {
                                        super.updateItem(item, empty)
                                        if (!empty && item != null) {
                                            text = item
                                            style = "-fx-padding: 0;"
                                            if (item.startsWith("+")) style = (style
                                                    + "-fx-background-color: #96FFC8; ") else if (item.startsWith("-")) style =
                                                (style
                                                        + "-fx-background-color: #FFAFA0;")
                                        }
                                    }
                                }
                            }
                        }
                    }
                    setDividerPositions(0.5)
                }
            }
            anchorpane {
                gridpane {
                    anchorpaneConstraints {
                        leftAnchor = 0.0
                        topAnchor = 0.0
                        rightAnchor = 0.0
                        bottomAnchor = 0.0
                    }
                    label("Commit Options") {
                        gridpaneConstraints {
                            marginBottom = 5.0
                            marginLeft = 5.0
                            marginRight = 5.0
                            marginTop = 5.0
                        }
                    }
                    textarea {
                        gridpaneConstraints {
                            rowIndex = 1
                            hAlignment = HPos.CENTER
                            rowSpan = 2147483647
                            vAlignment = VPos.CENTER
                        }
                        promptText = "Commit message"
                        prefWidth = 395.0
                        textProperty().bindBidirectional(commitBuilder.commitMessageProperty)
                    }
                    checkbox("Ammend last commit") {
                        gridpaneConstraints {
                            columnIndex = 1
                            columnSpan = 2
                        }
                        selectedProperty().bindBidirectional(commitBuilder.amendProperty)
                    }
                    button("Commit") {
                        gridpaneConstraints {
                            columnIndex = 1
                            columnSpan = 2
                            rowIndex = 1
                            marginLeft = 5.0
                            marginBottom = 5.0
                            marginRight = 5.0
                            marginTop = 5.0
                        }
                        action { commit(); }
                    }
                    button("Cancel") {
                        gridpaneConstraints {
                            columnIndex = 1
                            columnSpan = 2
                            rowIndex = 2
                        }
                        action { cancelCommit(); }
                    }
                    label("Author") {
                        gridpaneConstraints {
                            columnIndex = 1
                            columnSpan = 2
                            rowIndex = 4
                            marginBottom = 5.0
                            marginTop = 15.0
                        }
                    }
                    label("Name:") {
                        textAlignment = TextAlignment.RIGHT
                        gridpaneConstraints {
                            columnIndex = 1
                            rowIndex = 5
                            marginRight = 5.0
                        }
                    }
                    label("Email:") {
                        textAlignment = TextAlignment.RIGHT
                        gridpaneConstraints {
                            columnIndex = 1
                            rowIndex = 6
                            marginRight = 5.0
                        }
                    }
                    textfield {
                        prefWidth = 150.0
                        textProperty().bindBidirectional(commitBuilder.authorNameProperty)
                        gridpaneConstraints {
                            columnIndex = 2
                            rowIndex = 5
                            marginBottom = 5.0
                            marginLeft = 5.0
                            marginRight = 5.0
                            marginTop = 5.0
                        }
                    }
                    textfield {
                        prefWidth = 150.0
                        textProperty().bindBidirectional(commitBuilder.authorEmailProperty)
                        gridpaneConstraints {
                            columnIndex = 2
                            rowIndex = 6
                            marginBottom = 5.0
                            marginLeft = 5.0
                            marginRight = 5.0
                            marginTop = 5.0
                        }
                    }

                    constraintsForColumn(0).minWidth = 10.0
                    constraintsForColumn(0).hgrow = Priority.ALWAYS
                    constraintsForColumn(1).halignment = HPos.CENTER
                    constraintsForColumn(1).hgrow = Priority.SOMETIMES
                    constraintsForColumn(2).halignment = HPos.CENTER
                    constraintsForColumn(2).hgrow = Priority.SOMETIMES
                    constraintsForRow(0).minHeight = 10.0
                    constraintsForRow(0).vgrow = Priority.SOMETIMES
                    constraintsForRow(1).minHeight = 10.0
                    constraintsForRow(1).vgrow = Priority.SOMETIMES
                    constraintsForRow(2).minHeight = 10.0
                    constraintsForRow(2).vgrow = Priority.SOMETIMES
                    constraintsForRow(3).minHeight = 10.0
                    constraintsForRow(3).prefHeight = 30.0
                    constraintsForRow(3).vgrow = Priority.ALWAYS
                    constraintsForRow(4).minHeight = 10.0
                    constraintsForRow(4).prefHeight = 30.0
                    constraintsForRow(4).vgrow = Priority.SOMETIMES
                    constraintsForRow(5).minHeight = 10.0
                    constraintsForRow(5).prefHeight = 30.0
                    constraintsForRow(5).vgrow = Priority.SOMETIMES
                    constraintsForRow(6).minHeight = 10.0
                    constraintsForRow(6).prefHeight = 30.0
                    constraintsForRow(6).vgrow = Priority.SOMETIMES
                }
            }
            setDividerPositions(0.75)
        }

        applyClassRecursively(Button::class, "btn")
        applyClassRecursively(Label::class, "lbl")
//        applyClassRecursively(Pane::class, "panel")
    }


    private fun commit() {
        try {
            manager.commit(commitBuilder)
            commitBuilder.commitMessage = "";
            commitBuilder.amend = false;
        } catch (e: GitAPIException) {
            log.log(Level.SEVERE, "Error committing", e)
        }
    }

    private fun cancelCommit() {
        commitBuilder.commitMessage = "";
        commitBuilder.amend = false;
    }

    private fun unstageAll() {
        manager.unstageAll()
    }

    private fun stageAll() {
        manager.stageAll()
    }
}
