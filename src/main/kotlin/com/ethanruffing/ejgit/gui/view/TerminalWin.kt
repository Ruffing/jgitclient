package com.ethanruffing.ejgit.gui.view

import com.ethanruffing.ejgit.git.RepositoryManager
import com.kodedu.terminalfx.TerminalBuilder
import com.kodedu.terminalfx.config.TerminalConfig
import tornadofx.*
import java.nio.file.Path

class TerminalWin(val manager: RepositoryManager) : View("Git Terminal") {
    override val root = tabpane {
        prefWidth = 640.0
        prefHeight = 480.0
        var config = TerminalConfig();
        config.backgroundColor = "black"
        config.foregroundColor = "white"
        config.cursorColor = "white"
        //TODO: Add preferences for the terminal launcher command
        var builder = TerminalBuilder(config);
        builder.terminalPath = Path.of(manager.SelectedRepository.directory.toURI()).parent;
        tabs.add(builder.newTerminal());
    }
}
