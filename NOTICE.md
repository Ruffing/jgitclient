Third Party Libraries
=====================
EJGit uses several third-party libraries that are distributed under
different licenses.  These libraries and their respective licenses are listed
below:

| Library                | License                            |
| ---------------------- | ---------------------------------- |
| Commons IO             | [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)        |
| Commons Lang           | [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)        |
| Preference Abstraction | [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)        |
| JGit                   | [Eclipse Distribution License, v1.0](https://eclipse.org/org/documents/edl-v10.html) |
| JGit-Flow              | [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)        |
