EJGit
==========
Copyright 2015-2023 by Ethan Ruffing.

EJGit is a git client written in Kotlin. It uses
[JGit](http://www.eclipse.org/jgit/) as a backend and a JavaFX-based GUI.


Licensing
---------
EJGit is Copyright 2015-2023 by Ethan Ruffing. All rights reserved.

NO LICENSE HAS BEEN GRANTED TO USE, COPY, OR MODIFY THIS PROGRAM IN ANY FORM OR
FOR ANY PURPOSE.

EJGit uses several third-party libraries that are distributed under
different licenses.  These libraries and their respective licenses are listed
in [NOTICE.md](NOTICE.md).
